class DateTime{
	constructor(){
		this.daysText = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
		this.color = color(51);
	}

	drawTime(x, y, tSize){
		textAlign(CENTER);
		fill(this.color);
		textSize(tSize);
		text(this.leftPad(hour(), 2) + ":" + this.leftPad(minute(), 2) , x, y);
	}

	drawDay(x, y, tSize){
		textAlign(CENTER);
		fill(this.color);
		textSize(tSize);
		let dayOfTheWeek = new Date().getDay();
		text(this.daysText[dayOfTheWeek], x, y);
	}

	drawDate(x, y, tSize){
		textAlign(CENTER);
		fill(this.color);
		textSize(tSize);
		text(this.leftPad(day(), 2) + "/" + this.leftPad(month(), 2) + "/" + year() % 2000, x, y);
	}

	minInterval(minutes){
		if (minute() % minutes == 0 && second() == 0){
			return true;
		} else {
			return false;
		}
	}

	leftPad(number, targetLength) {
	    var output = number + '';
	    while (output.length < targetLength) {
	        output = '0' + output;
	    }
	    return output;
	}
}
