class Boid{
	constructor(x, y, rad){
		this.pos = createVector(x, y);
		this.vel = createVector(0, 0);
		this.acc = createVector(0, 0);
		this.radius = 5;
		this.perseiveRadius = rad;
		this.maxVel = 2;
		this.color = color(255);
		this.onEdges = true;
	}

	show(){
		push();
		noStroke();

		colorMode(HSB, 255);
		fill(this.color);

		translate(this.pos.x, this.pos.y);
		rotate(this.vel.heading() - PI/2);

		beginShape();
		vertex(-this.radius, -2*this.radius);
		vertex(this.radius, -2*this.radius);
		vertex(0, 2*this.radius);
		endShape(CLOSE);
		pop();
	}

	update(){
		this.pos.add(this.vel);
		this.vel.add(this.acc);
		this.acc.mult(0);
		this.vel.limit(this.maxVel);
		if (this.onEdges){
			this.edges();
		}

	}

	flock(boids){
		let cohesionSteer = createVector(0, 0);
		let alignmentSteer = createVector(0, 0);
		let seperationSteer = createVector(0, 0);

		let total = 0;
		for (let i = 0; i < boids.length; i++){
			let d = this.pos.dist(boids[i].pos);
			if (boids[i] != this && d < this.perseiveRadius){
				let diff = p5.Vector.sub(this.pos, boids[i].pos);
				diff.div(d);
				cohesionSteer.add(boids[i].pos);
				alignmentSteer.add(boids[i].vel);
				seperationSteer.add(diff);
				total ++;
			}
		}

		cohesionSteer.div(total);
		cohesionSteer.sub(this.pos);
		cohesionSteer.div(50); // another divide to slow things down
		this.addForce(cohesionSteer);

		alignmentSteer.div(total);
		alignmentSteer.div(10); // another divide to slow things down
		this.addForce(alignmentSteer);

		seperationSteer.div(total);
		seperationSteer.mult(0.9);
		this.addForce(seperationSteer);
	}

	addForce(force){
		this.acc.add(force);
	}

	avoid(obsticals){
		for (let i = 0; i < obsticals.length; i++){
			if (this.pos.dist(obsticals[i].pos) < obsticals[i].reflectionRadius){
				let v = this.vel.copy();
				let lead = p5.Vector.add(this.pos, v.setMag(obsticals[i].reflectionRadius));

				let steer = p5.Vector.sub(lead, obsticals[i].pos);
				steer.limit(0.4);
				this.addForce(steer);
			}
		}
	}

	removeOnEdges(boids, i){
		let offset = 30;
		if ((this.pos.x > width + offset || this.pos.x < -offset ||
			this.pos.y > height + offset || this.pos.y < -offset)){
			boids.splice(i, 1);
		}
	}

	edges(){
		let offset = 30;
		if (this.pos.x > width + offset){
			this.pos.x = -offset;
		} else if (this.pos.x < -offset){
			this.pos.x = width + offset;
		}
		if (this.pos.y > height + offset){
			this.pos.y = -offset;
		} else if (this.pos.y < -offset){
			this.pos.y = height + offset;
		}
	}
}
