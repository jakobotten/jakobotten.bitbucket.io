class Obstical{
	constructor(x, y, r){
		this.pos = createVector(x, y);
		this.radius = r;
		this.reflectionRadius = r + 30;
		this.color = color(100);
	}

	show(){
		noStroke();
		fill(this.color);
		ellipse(this.pos.x, this.pos.y, 2 * this.radius);
	}
}
