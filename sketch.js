let boids = [];
let init = 1;
let obsticals = [];
let maxBoids = 255;
let reset = false;
let dateTime;

function setup() {
 	createCanvas(windowWidth, windowHeight);
	colorMode(HSB, 255);

	dateTime = new DateTime();

	obsticals.push(new Obstical(width/3, height/3 - 45, 160));
	obsticals.push(new Obstical(width*3/4, height*3/4 , 100));
	obsticals.push(new Obstical(width*1/5, height*5/6 , 30));
	obsticals.push(new Obstical(width*2/5, height*3/4 , 20));
	obsticals.push(new Obstical(width*3/5, height*2/5 , 40));
	obsticals.push(new Obstical(width*5/6, height*1/5 , 30));
	obsticals.push(new Obstical(width*2/5, height*3/4 , 20));

}

function draw() {
 	background(51);

	// Draw the obsticals
	for (let i = 0; i < obsticals.length; i++){
		obsticals[i].show();
	}

	// Draw the time in different locations
	dateTime.drawDay(width*3/4, height*3/4, 60);
	dateTime.drawDate(width*3/4, height*3/4 + 40, 40);
	dateTime.drawTime(width/3, height/3, 120);

	// Create a new boids until maxed out
	if (boids.length < maxBoids &! reset){
		generateBoid(boids.length);
	}

	// Update the boids dynamics
	for (let i = 0; i < boids.length; i++){
		boids[i].flock(boids);
		boids[i].avoid(obsticals);
		boids[i].update();
		boids[i].show();

		// If reset, destroy boids as they leave the window
		if (reset){
			boids[i].onEdges = false;
			boids[i].removeOnEdges(boids, i);
		}
	}

	if (dateTime.minInterval(5)){
		reset = true;
	}

	// Clear the reset flag if number of boids is zero
	if (reset && boids.length == 0){
		reset = false;
	}
}

function mouseClicked(){
	reset = true;
}

function mouseWheel(event) {
	// Disable scrolling
  	return false;
}

function generateBoid(i){
	let offset = 30;

	if (i%4 == 0){
		boids.push(new Boid(random(0, width), -offset, 60));
	} else if (i%4 == 1) {
		boids.push(new Boid(random(0, width), height + offset, 60));
	} else if (i%4 == 2) {
		boids.push(new Boid(-offset, random(0 , height), 60));
	} else {
		boids.push(new Boid(width + offset, random(0 , height), 60));
	}

	let thisBoid = boids[boids.length - 1];

	thisBoid.color = color(i%255, 100, 200);
	thisBoid.vel.x = random(-init, init);
	thisBoid.vel.y = random(-init, init);
	thisBoid.onEdges = true;
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
